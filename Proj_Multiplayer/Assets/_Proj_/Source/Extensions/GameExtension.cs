﻿using Source.Architecture.EntryPoints;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Source.Extensions
{
public static class GameExtensions
{
    public static T FindEntryPoint<T>(this Scene scene) where T : SceneEntryPoint
    {
        var rootGameObjects = scene.GetRootGameObjects();

        SceneEntryPoint entryPoint = null;
			
        foreach (var rootGameObject in rootGameObjects)
        {
            var entryPoints = rootGameObject.GetComponentsInChildren<SceneEntryPoint>();
				
            foreach (var sceneEntryPoint in entryPoints)
            {
                if (entryPoint == null)
                    entryPoint = sceneEntryPoint;
                else
                    Debug.LogWarning("There are multiple SceneEntryPoint scripts in scene: " + scene.name);
            }
        }

        return entryPoint != null ? (T) entryPoint : null;
    }
}
}