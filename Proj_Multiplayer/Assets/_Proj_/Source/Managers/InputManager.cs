using System;
using Source.Helpers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

namespace Source.Managers
{
public class InputManager : Singleton<InputManager>
{
#region Private fields

    int _lockCount;
    EventSystem _eventSystem;
    BaseInputModule _inputModule;

    public bool EventIsEnable => _eventSystem.enabled;
    bool InputEnable
    {
        set
        {
            _lockCount = !value ? _lockCount + 1 : Mathf.Max(0, _lockCount - 1);
            _inputModule.enabled = _lockCount == 0;
            _eventSystem.enabled = _lockCount == 0;

            Debug.Log($"[InputManager] {(!value ? "Lock" : "Unlock")} request, system is {_inputModule.enabled}!");
        }
    }

#endregion

#region Events

    public event Action OnInputLocked;

#endregion

#region Implementation

    public IDisposable LockInputSystem()
    {
        OnInputLocked?.Invoke();
        return new ReleaseWrapper();
    }

    public void TempLockInput() { }

    void Init()
    {
        _inputModule = GetComponent<BaseInputModule>();
        _eventSystem = GetComponent<EventSystem>();

        Assert.IsNotNull(_inputModule, "No InputModule  found.");
        Input.multiTouchEnabled = false; // HACK: disable multi touch
    }

#endregion

#region Lifecycle

    void Awake()
    {
        RegisterSingleton(this);
        Init();
    }

#endregion

#region Data

    class ReleaseWrapper : IDisposable
    {
        public ReleaseWrapper() => Instance.InputEnable = false;
        public void Dispose() => Instance.InputEnable = true;
    }

#endregion
}
}