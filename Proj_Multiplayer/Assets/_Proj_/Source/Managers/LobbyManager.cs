using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Source.Helpers;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;

namespace Source.Managers
{
public class LobbyManager : MonoBehaviour
{
#region Fields

    [SerializeField] RelayManager _relayManager;

    Lobby _hostLobby;
    Lobby _joinedLobby;
    int _maxPlayer = 4;
    bool _isLobbyHost;
    float _heartbeatLobby;
    readonly float _heartbeatLobbyTimer = 15;

#endregion

#region Events & handlers

    public Action<string> OnSomethingWrong;
    void OnSomethingWrong_handler(string message) => OnSomethingWrong?.Invoke(message);

#endregion

#region Monobeh

    async void Start()
    {
        using (InputManager.Instance.LockInputSystem())
        {
            await UnityServices.InitializeAsync();

            AuthenticationService.Instance.SignedIn += () =>
                Debug.Log($"----- Sign in {AuthenticationService.Instance.PlayerId}");

            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
    }

    void Update() => HandleLobbyHeartbeat();

#endregion

#region Public methods

    public async void CreateLobby(string lobbyName, string playerName)
    {
        using (InputManager.Instance.LockInputSystem())
        {
            try
            {
                var createLobbyOptions = new CreateLobbyOptions
                {
                    IsPrivate = false,
                    Player = GetPlayerInfo(playerName),
                    Data = new Dictionary<string, DataObject>
                    {
                        {Constants.LobbyOptions.KEY_START_GAME, new DataObject(DataObject.VisibilityOptions.Member, "0")}
                    }
                };

                var createdLobby = await LobbyService.Instance.CreateLobbyAsync(lobbyName, _maxPlayer, createLobbyOptions);

                _hostLobby = createdLobby;
                _joinedLobby = _hostLobby;
                Debug.Log($"----- Created lobby {_joinedLobby.Name}    {_joinedLobby.MaxPlayers}");
                PrintPlayers(_joinedLobby);
            }
            catch (LobbyServiceException e)
            {
                Console.WriteLine(e);
            }
        }
    }

    public async void JoinLobby(string lobbyName, string playerName)
    {
        using (InputManager.Instance.LockInputSystem())
        {
            var lobbies = await GetLobbiesList();

            if (lobbies.Count == 0)
            {
                OnSomethingWrong_handler($"There's no empty lobby with name {lobbyName}");
                return;
            }

            foreach (var lobby in lobbies)
            {
                if (lobby.Name.Equals(lobbyName))
                {
                    if (!PlayerNameIsAvailable(lobby, playerName))
                    {
                        OnSomethingWrong_handler($"Name {playerName} is already taken in lobby {lobbyName}");
                        return;
                    }

                    var joinLobbyByIdOptions = new JoinLobbyByIdOptions();
                    joinLobbyByIdOptions.Player = GetPlayerInfo(playerName);
                    var joinedLobby = await Lobbies.Instance.JoinLobbyByIdAsync(lobby.Id, joinLobbyByIdOptions);

                    _joinedLobby = joinedLobby;
                    PrintPlayers(lobby);
                    Debug.Log($"----- Joined lobby {lobby.Name}");
                    return;
                }
            }

            OnSomethingWrong_handler($"There's no empty lobby with name {lobbyName}");
        }
    }

    bool PlayerNameIsAvailable(Lobby lobby, string joinedPlayerName)
    {
        var players = lobby.Players;

        foreach (var player in players)
            if (player.Data[Constants.LobbyOptions.PLAYER_NAME].Value == joinedPlayerName)
                return true;

        return false;
    }

    public async void LeaveLobby()
    {
        if(_joinedLobby==null)
            return;

        try
        {
            await Lobbies.Instance.RemovePlayerAsync(_joinedLobby.Id, AuthenticationService.Instance.PlayerId);
            
            _joinedLobby = null;
        }
        catch (LobbyServiceException e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    public async void StartGame()
    {
        if (!_isLobbyHost)
            return;
        
        try
        {
            string relayCode = await _relayManager.CreateRelay();

            var lobby = await Lobbies.Instance.UpdateLobbyAsync(_joinedLobby.Id, new UpdateLobbyOptions()
            {
                Data = new Dictionary<string, DataObject>
                {
                    {Constants.LobbyOptions.KEY_START_GAME, new DataObject(DataObject.VisibilityOptions.Member, relayCode)}
                }
            });

            _joinedLobby = lobby;
        }
        catch (LobbyServiceException e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

#endregion

#region Private methods

    async Task<List<Lobby>> GetLobbiesList()
    {
        using (InputManager.Instance.LockInputSystem())
        {
            try
            {
                var queryLobbiesOptions = new QueryLobbiesOptions()
                {
                    Count = 4,
                    Filters = new List<QueryFilter> {new(QueryFilter.FieldOptions.AvailableSlots, "0", QueryFilter.OpOptions.GT)},
                    Order = new List<QueryOrder> {new(false, QueryOrder.FieldOptions.Created)}
                };

                var response = await Lobbies.Instance.QueryLobbiesAsync(queryLobbiesOptions);

                return response.Results;
            }
            catch (LobbyServiceException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    Player GetPlayerInfo(string playerName)
    {
        var player = new Player(id: AuthenticationService.Instance.PlayerId,
                                data: new Dictionary<string, PlayerDataObject>()
                                {
                                    {Constants.LobbyOptions.PLAYER_NAME, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, playerName)}
                                });

        return player;
    }

    void PrintPlayers(Lobby lobby)
    {
        Debug.Log($"----- Players in lobby {lobby.Name} - {lobby.Players.Count}");

        foreach (var player in lobby.Players)
            Debug.Log($"----- Player - {player.Id}   {player.Data[Constants.LobbyOptions.PLAYER_NAME].Value}");
    }

    async void HandleLobbyHeartbeat()
    {
        if (_hostLobby == null)
            return;

        _heartbeatLobby += Time.deltaTime;

        if (_heartbeatLobby >= _heartbeatLobbyTimer)
        {
            _heartbeatLobby = 0;

            await LobbyService.Instance.SendHeartbeatPingAsync(_hostLobby.Id);
        }
    }

#endregion
}
}