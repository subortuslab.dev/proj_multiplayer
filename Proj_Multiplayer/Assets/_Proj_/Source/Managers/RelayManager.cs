﻿using System;
using System.Threading.Tasks;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;

namespace Source.Managers
{
public class RelayManager : MonoBehaviour
{
    public async Task<string> CreateRelay()
    {
        try
        {
            var allocation = await RelayService.Instance.CreateAllocationAsync(4);
            var joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);
            Debug.Log($"----   {joinCode}");
        
            var relayServerData = new RelayServerData(allocation, "dtls");
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayServerData);
            NetworkManager.Singleton.StartHost();
        
            return joinCode;
        }
        catch (RelayServiceException e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    public async void JoinRelay(string relayCode)
    {
        try
        {
            Debug.Log($"----   Join relay {relayCode}");
            var joinAllocation = await RelayService.Instance.JoinAllocationAsync(relayCode);
            var relayServerData = new RelayServerData(joinAllocation, "dtls");
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayServerData);
            NetworkManager.Singleton.StartClient();
        }
        catch (RelayServiceException e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}
}