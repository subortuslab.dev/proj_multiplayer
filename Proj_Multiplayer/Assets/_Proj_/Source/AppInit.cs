﻿using Source.Architecture.Interfaces;
using Source.Architecture.States;
using Source.Interface;
using Source.Services;
using UnityEngine;

namespace Source
{
public class AppInit : MonoBehaviour
{
    StatesMachine _stateMachine;
    ISceneLoaderService _sceneLoaderService;

    void Awake()
    {
        _sceneLoaderService = new SceneLoaderService();

        var states = new IStateInfo[]
        {
            new StateInit(),
            new StateLobby(_sceneLoaderService)
        };

        _stateMachine = new StatesMachine(states);
        _stateMachine.Enter<StateInit>();
    }

    void Update() => _stateMachine.UpdateTick();
}
}