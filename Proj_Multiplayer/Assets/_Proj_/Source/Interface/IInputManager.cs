﻿using System;

namespace Source.Interface
{
public interface IInputManager
{
    IDisposable LockInputSystem();
    void TempLockInput();
}
}