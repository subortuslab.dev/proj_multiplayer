﻿using System.Threading.Tasks;
using Source.Services;
using UnityEngine.SceneManagement;

namespace Source.Interface
{
public interface ISceneLoaderService : IService
{
    Task<SceneLoadResult> LoadSceneAsync(string sceneName, LoadSceneParameters parameters, bool setActiveScene=false);
    Task UnloadSceneAsync(string sceneName);
}
}