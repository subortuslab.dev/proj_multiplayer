﻿namespace Source.Helpers
{
public static class Constants
{
    public static class LobbyOptions
    {
        public const string PLAYER_NAME = "PlayerName";
        public const string KEY_START_GAME = "0";
    }

    public class SceneNames
    {
        public const string INIT_SCENE = "InitScene";
        public const string LOADING_SCENE = "LoadingScene";
        public const string LOBBY_SCENE = "LobbyScene";
    }
}
}