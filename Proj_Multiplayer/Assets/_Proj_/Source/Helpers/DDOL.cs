using UnityEngine;

namespace Source.Helpers
{
public class DDOL : MonoBehaviour
{
    public static DDOL Instance;
    
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
}
