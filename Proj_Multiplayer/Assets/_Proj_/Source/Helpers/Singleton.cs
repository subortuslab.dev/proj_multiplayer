﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Source.Helpers
{
public abstract class StaticInstance<T> : MonoBehaviour where T : MonoBehaviour 
{
    static T _instance = null;

    public static T Inst
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                if (_instance == null)
                {
                    var singletonObj = new GameObject();
                    singletonObj.name = typeof(T).ToString();
                    _instance = singletonObj.AddComponent<T>();
                }
            }

            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = GetComponent<T>();
        DontDestroyOnLoad(gameObject);
    }
}

public class Singleton<T> : MonoBehaviour where T : class
{
    static T _instance;

    /// <summary>
    /// As simple as that - access the single instance, feel free to cache it in short name say
    /// var cm = CoolManager.Instance, but DO NOT store such cached instances in statics
    /// or any long-lived objects, its error prone - we did it, we know ;)
    /// </summary>
    public static T Instance
    {
        get
        {
            if (_instance == null)
                Debug.LogError("Singleton error! make sure you have a " + typeof(T).FullName + " in your scene");

            return _instance;
        }
    }

    /// <summary>
    /// Use this to check if singleton did RegisterSingleton already, class method
    /// </summary>
    public static bool Initialized() => _instance != null;

    /// <summary>
    ///Hack method to reset the instance, if say scene is reloaded and want new copy of singleton
    /// </summary>
    public static void ResetInstance() => _instance = null;

    /// <summary>
    /// Call this in order to register singleton, usually in Awake(), use RootSingleton to define order,
    /// do not use script orded override in Unity editor, it's prone to errors
    /// </summary>
    public void RegisterSingleton(T instance)
    {
        Assert.IsNull(_instance);
        Assert.IsNotNull(instance);
        _instance = instance;
    }
}
}