﻿using UnityEngine.SceneManagement;

namespace Source.Services
{
public struct SceneLoadResult
{
    public Scene Scene;
    public bool Success;
    public bool AlreadyLoaded;
}
}