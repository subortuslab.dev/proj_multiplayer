﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Source.Interface;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Source.Services
{
public class SceneLoaderService : ISceneLoaderService
{
    readonly Dictionary<string, Scene> _loadedScenes = new();

    public async Task<SceneLoadResult> LoadSceneAsync(string sceneName, LoadSceneParameters parameters, bool setActiveScene)
    {
        var tcs = new TaskCompletionSource<SceneLoadResult>();

        if (_loadedScenes.ContainsKey(sceneName))
        {
            SetTaskCompletionSource(tcs, _loadedScenes[sceneName], true, true);
        }
        else
        {
            var asyncOperation = SceneManager.LoadSceneAsync(sceneName, parameters);
            asyncOperation.completed += _ =>
            {
                var scene = SceneManager.GetSceneByName(sceneName);

                if (setActiveScene)
                    SceneManager.SetActiveScene(scene);

                _loadedScenes[sceneName] = scene;
                SetTaskCompletionSource(tcs, _loadedScenes[sceneName], true, false);
            };
        }

        return await tcs.Task;
    }

    public async Task UnloadSceneAsync(string sceneName)
    {
        try
        {
            var tcs = new TaskCompletionSource<bool>();

            var asyncOperation = SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(sceneName));
            asyncOperation.completed += _ =>
            {
                if (_loadedScenes.ContainsKey(sceneName))
                    _loadedScenes.Remove(sceneName);

                tcs.TrySetResult(true);
            };

            await tcs.Task;
        }
        catch (Exception e)
        {
            Debug.LogError($"Something happened during scene unloading: {e.Message}.");
        }
    }

    void SetTaskCompletionSource(TaskCompletionSource<SceneLoadResult> tcs, Scene scene, bool isSuccess, bool isAlreadyLoaded)
    {
        tcs.TrySetResult(new SceneLoadResult()
        {
            Scene = scene,
            Success = isSuccess,
            AlreadyLoaded = isAlreadyLoaded
        });
    }
}
}