﻿using Source.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Source.UI
{
public class LobbyUI : MonoBehaviour
{
    [SerializeField] TMP_InputField _txtPlayerName;
    [SerializeField] TMP_InputField _txtCreateLobby;
    [SerializeField] Button _btnCreateLobby;
    [SerializeField] TMP_InputField _txtJoinLobby;
    [SerializeField] Button _btnJoinLobby;
    
    [Space]
    [SerializeField] LobbyManager lobbyManager;

    void Awake()
    {
        _btnCreateLobby.onClick.AddListener(CreateLobby);
        _btnJoinLobby.onClick.AddListener(JoinLobby);
    }

    void OnDestroy()
    {
        _btnCreateLobby.onClick.RemoveListener(CreateLobby);
        _btnJoinLobby.onClick.RemoveListener(JoinLobby);
    }

    void SetupUIOnEnterLobby()
    {
        //TODO: Set Player name on EnterLobby (empty or already set)
    }

    void CreateLobby()
    {
        if (InputFieldIsEmpty(_txtCreateLobby) || InputFieldIsEmpty(_txtPlayerName))
            return;

        lobbyManager.CreateLobby(_txtCreateLobby.text.ToUpper(), _txtPlayerName.text.ToUpper());
    }

    void JoinLobby()
    {
        if (InputFieldIsEmpty(_txtJoinLobby))
            return;

        lobbyManager.JoinLobby(_txtJoinLobby.text.ToUpper(), _txtPlayerName.text.ToUpper());
    }

    bool InputFieldIsEmpty(TMP_InputField inputField) => string.IsNullOrEmpty(inputField.text);
}
}