﻿using Source.Architecture.States;

namespace Source.Architecture.Interfaces
{
public interface IStateInfo
{
  bool IsActive { get; set; }
  StatesMachine StatesMachine { get; set; }
}
}