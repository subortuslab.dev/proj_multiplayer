﻿namespace Source.Architecture.Interfaces
{
public interface IStateExit
{
    void Exit();
}
}