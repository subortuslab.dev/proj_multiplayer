﻿namespace Source.Architecture.Interfaces
{
public interface IStateUpdatable
{
    void Update();
}
}