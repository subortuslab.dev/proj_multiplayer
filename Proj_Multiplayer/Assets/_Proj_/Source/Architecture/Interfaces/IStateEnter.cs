﻿namespace Source.Architecture.Interfaces
{
public interface IStateEnter : IStateExit, IStateInfo
{
    void Enter();
}
}