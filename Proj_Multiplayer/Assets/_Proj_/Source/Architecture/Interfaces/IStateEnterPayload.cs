﻿namespace Source.Architecture.Interfaces
{
public interface IStateEnterPayload<in TPayload> : IStateExit, IStateInfo
{
    void Enter(TPayload payload);
}
}