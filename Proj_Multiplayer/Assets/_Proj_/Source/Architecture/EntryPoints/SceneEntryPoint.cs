﻿using UnityEngine;

namespace Source.Architecture.EntryPoints
{
public abstract class SceneEntryPoint : MonoBehaviour
{
    public abstract void Enter();
    public abstract void Exit();
}
}