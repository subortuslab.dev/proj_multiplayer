﻿using System.Threading.Tasks;
using Source.Architecture.EntryPoints;
using Source.Architecture.Interfaces;
using Source.Extensions;
using Source.Helpers;
using Source.Interface;
using UnityEngine.SceneManagement;

namespace Source.Architecture.States
{
public class StateLobby : IStateEnter
{
    ISceneLoaderService _sceneProvider;

    public bool IsActive { get; set; }
    public StatesMachine StatesMachine { get; set; }

    public StateLobby(ISceneLoaderService sceneProvider)
    {
        _sceneProvider = sceneProvider;
    }

    public void Enter() => LoadSplashScreen().GetAwaiter();

    public void Exit() { }

    async Task LoadSplashScreen()
    {
        await _sceneProvider.LoadSceneAsync(Constants.SceneNames.LOADING_SCENE, new LoadSceneParameters(LoadSceneMode.Additive), false);
        
        _sceneProvider.UnloadSceneAsync(Constants.SceneNames.INIT_SCENE).GetAwaiter();
        var sceneLoadResult = await _sceneProvider.LoadSceneAsync(Constants.SceneNames.LOBBY_SCENE, new LoadSceneParameters(LoadSceneMode.Additive), true);
        
        var entryPoint = sceneLoadResult.Scene.FindEntryPoint<LobbyEntryPoint>();
        entryPoint.Enter();

        //TODO: 1) Load SplashScreen
        //TODO: 2) Get progress UI Objects (text, bar)
        //TODO: 3) Load Lobby scene async
        //TODO: 4) Store all elements for set progress bar
        //TODO: 5) Subscribe on lobby scene to update progress bar during loading scene and objects
        //TODO: 6) When everything is finished unload loading screen

    }
}
}