﻿using System;
using System.Collections.Generic;
using Source.Architecture.Interfaces;

namespace Source.Architecture.States
{
[Serializable]
public class StatesMachine
{
    readonly Dictionary<Type, IStateInfo> _states;
    readonly Dictionary<Type, IStateUpdatable> _updatableStates;

    protected IStateInfo CurrentStateInfo { get; set; }

    public StatesMachine(params IStateInfo[] states)
    {
        _states = new Dictionary<Type, IStateInfo>();
        _updatableStates = new Dictionary<Type, IStateUpdatable>();

        foreach (var state in states)
        {
            if (state is IStateUpdatable updatableState && !_updatableStates.ContainsKey(updatableState.GetType()))
                _updatableStates.Add(updatableState.GetType(), updatableState);

            if (_states.ContainsKey(state.GetType()))
                continue;

            state.StatesMachine = this;
            _states.Add(state.GetType(), state);
        }
    }

    public bool IsCurrentStateOfType<T>()
        => CurrentStateInfo is T;

    public virtual void Enter<TState>(Action onLoaded = null) where TState : class, IStateEnter
    {
        var state = ChangeState<TState>();
        state.Enter();
    }

    public virtual void Enter<TState, TPayload>(TPayload payload) where TState : class, IStateEnterPayload<TPayload>
    {
        var state = ChangeState<TState>();
        state.Enter(payload);
    }

    protected virtual TState ChangeState<TState>() where TState : class, IStateInfo
    {
        if (CurrentStateInfo != null)
            CurrentStateInfo.IsActive = false;

        if (CurrentStateInfo is IStateExit exitState)
            exitState.Exit();

        var state = GetState<TState>();
        state.IsActive = true;
        CurrentStateInfo = state;
        return state;
    }

    protected virtual TState GetState<TState>() where TState : class, IStateInfo => _states[typeof(TState)] as TState;

    public void UpdateTick()
    {
        foreach (var state in _updatableStates.Values)
            state.Update();
    }
}
}