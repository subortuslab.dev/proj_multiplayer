﻿using Source.Architecture.Interfaces;

namespace Source.Architecture.States
{
public class StateInit : IStateEnter
{
    public bool IsActive { get; set; }
    public StatesMachine StatesMachine { get; set; }
    public void Enter() => StatesMachine.Enter<StateLobby>();
    public void Exit() { }
}
}